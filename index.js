var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var app = express();
var jogadores = [
    { playerName: 'AAA', data: Date.now(), score: 1000 },
    { playerName: 'BBB', data: Date.now(), score: 2000 },
    { playerName: 'CCC', data: Date.now(), score: 3000 },
    { playerName: 'DDD', data: Date.now(), score: 2500 },
    { playerName: 'EEE', data: Date.now(), score: 1000 },
];

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/jogadores', function (req, res) {
    jogadores.sort(function (a, b) {
        return b.score - a.score;
    });

    res.json(jogadores);
});

app.post('/jogadores', function (req, res) {
    jogadores.push({
        playerName: req.body.nome,
        score: req.body.pontuacao,
        data: Date.now()
    });

    res.json({
        success: true,
        message: 'Pontuação cadastrada com sucesso!'
    });
});

app.listen(3000, function () {
    console.log('Serviço Iniciado');
});